import {
  Row,
  Col,
  Divider,
  Layout,
  Form,
  Input,
  Button,
  Checkbox,
  Tabs
} from "antd";
import { useHistory } from "react-router-dom";

const { Header, Content, Footer, Sider } = Layout;

const LoginComponent = () => {
  let history = useHistory();

  const onFinish = (values) => {
    console.log("Success:", values);
    history.push("/welcome");
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <Row justify="center" align="middle">
      <Col span={8}>
        <Form name="basic" onFinish={onFinish} onFinishFailed={onFinishFailed}>
          <Form.Item
            labelCol={{ span: 4 }}
            labelAlign="left"
            label="Email"
            name="email"
            rules={[
              {
                message: "Please input your email!",
                type: "email"
              }
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            labelCol={{ span: 4 }}
            labelAlign="left"
            label="Password"
            name="password"
            rules={[{ message: "Please input your password!" }]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item>
            <Col
              span={12}
              offset={6}
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <Button type="primary" htmlType="submit">
                Login
              </Button>
            </Col>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
};

export default LoginComponent;
