import { Row, Col, Form, Input, Button, DatePicker } from "antd";

const LoginComponent = () => {
  const onFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <Row justify="center" align="middle">
      <Col span={8}>
        <Form
          name="basic"
          layout="vertical"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="Username"
            name="username"
            rules={[{ required: true, message: "Please input your username!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            labelCol={{ span: 4 }}
            label="Email"
            name="email"
            rules={[
              {
                required: true,
                message: "Please input your email!",
                type: "email"
              }
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Tanggal lahir"
            name="birthday"
            rules={[
              {
                required: true,
                message: "Please input your birthday!"
              }
            ]}
          >
            <DatePicker />
          </Form.Item>
          <Form.Item
            label="Alamat"
            name="address"
            rules={[
              {
                required: true,
                message: "Please input your address!",
                type: "email"
              }
            ]}
          >
            <Input.TextArea />
          </Form.Item>
          <Form.Item
            label="Password"
            name="password1"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            label="Ulangi Password"
            name="password2"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item>
            <Col
              span={12}
              offset={6}
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <Button type="primary" htmlType="submit">
                Register
              </Button>
            </Col>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
};

export default LoginComponent;
