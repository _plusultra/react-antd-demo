import React from "react";
import { useLocation, useParams } from "react-router-dom";
const queryString = require("query-string");

const userDummyData = [
  { id: "123", name: "rakamin1", age: 23 },
  { id: "345", name: "rakamin2", age: 17 },
  { id: "567", name: "rakamin3", age: 40 },
  { id: "901", name: "rakamin4", age: 35 }
];

const WecomeComponent = ({ user }) => <div>{`Welcome, ${user.name}`}</div>;

const WelcomePage = () => {
  const { search } = useLocation();
  const { userid } = useParams();
  const parsedQueryString = queryString.parse(search);

  const usersFiltered = React.useMemo(() => {
    if (parsedQueryString.minAge) {
      return userDummyData.filter(
        (user) => parseInt(parsedQueryString.minAge) <= user.age
      );
    }
    return userDummyData;
  }, [parsedQueryString]);

  const specifiedUserData = React.useMemo(() => {
    if (typeof userid === "string") {
      return userDummyData.find((user) => user.id === userid);
    } else {
      return null;
    }
  }, [userid]);

  return specifiedUserData ? (
    <WecomeComponent user={specifiedUserData} />
  ) : (
    <ol>
      {usersFiltered.map((user) => (
        <li>
          <WecomeComponent user={user} />
        </li>
      ))}
    </ol>
  );
};

export default WelcomePage;
