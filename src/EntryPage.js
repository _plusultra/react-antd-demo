import { Tabs } from "antd";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import LoginComponent from "./LoginComponent";
import RegisterComponent from "./RegisterComponent";
import WelcomePage from "./WelcomePage";
import TransactionPage from "./TransactionPage";

const EntryPage = () => {
  return (
    <Switch>
      <Route exact path="/">
        <div>home</div>
      </Route>
      <Route exact path="/login">
        <LoginComponent />
      </Route>
      <Route exact path="/register">
        <RegisterComponent />
      </Route>
      <Route exact path="/welcome/:userid?">
        <WelcomePage />
      </Route>
      <Route exact path="/transaction">
        <TransactionPage />
      </Route>
    </Switch>
  );
};

export default EntryPage;
