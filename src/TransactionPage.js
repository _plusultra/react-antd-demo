import React from "react";
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  DatePicker,
  Select,
  InputNumber,
  Space,
  Divider,
  Menu
} from "antd";
const { Option } = Select;

const TransactionOptionsValue = [
  {
    key: "laku-pandai",
    value: "disabled",
    label: "Laku Pandai",
    isDisabled: true
  },
  {
    key: "cash-in-out",
    value: "cash in & out",
    label: "Cash in & out",
    isDisabled: false
  },
  {
    key: "report",
    value: "report",
    label: "Report",
    isDisabled: false
  },
  {
    key: "divider",
    value: "divider",
    label: "divider",
    isDisabled: false
  },
  {
    key: "tarik-tunai",
    value: "tarik-tunai",
    label: "Tarik Tunai",
    isDisabled: false
  }
];

const DataProvinsi = [
  {
    name: "Jawa Barat",
    kabupaten: [
      {
        name: "Kota Bandung",
        kecamatan: [
          "Coblong",
          "Lengkong Gudang",
          "Buahbatu",
          "Ujung Berung",
          "Antapani"
        ]
      },
      {
        name: "Bogor",
        kecamatan: ["Bojong Gede", "Ciawi", "Cibinong", "Cileungsi", "Cijeruk"]
      },
      {
        name: "Tasikmalaya",
        kecamatan: ["Bantarkalong", "Bojongasih", "Cibalong", "Cipatujah"]
      }
    ]
  },
  {
    name: "Jawa Tengah",
    kabupaten: [
      {
        name: "Grobogan",
        kecamatan: [
          "Brati",
          "Gabus",
          "Geyer",
          "Karangrayu",
          "Kradenan",
          "Purwodadi"
        ]
      },
      {
        name: "Kudus",
        kecamatan: ["Jekulo", "Kaliwungu", "Kudus", "Mejobo", "Undaan"]
      },
      {
        name: "Pekalongan",
        kecamatan: [
          "Bojong",
          "Buaran",
          "Karangayar",
          "Kedungwungi",
          "Lebakbarang"
        ]
      },
      {
        name: "Semarang",
        kecamatan: [
          "Ambarawa",
          "Bancak",
          "Bawen",
          "Banyubiru",
          "Bringin",
          "Getasan"
        ]
      }
    ]
  },
  {
    name: "Jawa Timur",
    kabupaten: [
      {
        name: "Kota Surabaya",
        kecamatan: ["Gubeng", "Kenjeran", "Tambaksari", "Tandes", "Wonokromo"]
      },
      {
        name: "Kota Malang",
        kecamatan: ["Blimbingan", "Kedungkandang", "Lowokwaru", "Sukun"]
      },
      { name: "Kota Madiun", kecamatan: ["Kartoharjo", "Manguharjo", "Taman"] },
      { name: "Kota Kediri", kecamatan: ["Kediri", "Mojoroto", "Pesantren"] },
      {
        name: "Banyuwangi",
        kecamatan: [
          "Pesanggaran",
          "Purwoharjo",
          "Muncar",
          "Genteng",
          "Glenmore",
          "Rogojampi"
        ]
      }
    ]
  },
  {
    name: "Bali",
    kabupaten: [
      {
        name: "Buleleng",
        kecamatan: ["Buleleng", "Gerokak", "Sawan", "Seririt", "Tejakula"]
      },
      {
        name: "Kota Denpasar",
        kecamatan: [
          "Denpasar Barat",
          "Denpasar Selatan",
          "Denpasar Timur",
          "Denpasar Utara"
        ]
      },
      {
        name: "Gianyar",
        kecamatan: ["Blahbatu", "Giayar", "Payangan", "Sukawati", "Ubud"]
      },
      {
        name: "Jembrana",
        kecamatan: ["Jembrana", "Melaya", "Negara", "Pekutaan"]
      },
      {
        name: "Tabanan",
        kecamatan: [
          "Baturiti",
          "Kerambitan",
          "Selemadeng Barat",
          "Selemadeng Timur",
          "Tabanan"
        ]
      }
    ]
  },
  {
    name: "Maluku",
    kabupaten: [
      {
        name: "Kota Ambon",
        kecamatan: ["Nusaniwe", "Sirimau", "Teluk Ambon", "Leitimur Selatan"]
      },
      {
        name: "Kota Tual",
        kecamatan: [
          "Pulau Dullah Utara",
          "Pulau Dullah Selatan",
          "Tayando Tam",
          "Pulau-Pulau Kur"
        ]
      },
      {
        name: "Buru",
        kecamatan: ["Namlea", "Air Buaya", "Waeapo", "Waplau", "Lolong Guba"]
      },
      {
        name: "Kepulauan Aru",
        kecamatan: ["Aru Selatan", "Aru Tengah", "Aru Utara"]
      }
    ]
  }
];

const TransactionPage = () => {
  const [selectedProvinsi, setSelectedProvinsi] = React.useState(null);
  const [selectedKabupaten, setSelectedKabupaten] = React.useState(null);
  const [selectedKecamatan, setSelectedKecamatan] = React.useState(null);

  const onFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const handleSelectedProvinsi = (value) => {
    setSelectedProvinsi(value);
  };

  const handleSelectedKabupaten = (value) => {
    setSelectedKabupaten(value);
  };

  const handleSelectedKecamatan = (value) => {
    setSelectedKecamatan(value);
  };

  const dataKabupaten = React.useMemo(() => {
    return (
      (DataProvinsi.find((provinsi) => provinsi.name === selectedProvinsi) &&
        DataProvinsi.find((provinsi) => provinsi.name === selectedProvinsi)
          .kabupaten) ||
      []
    );
  }, [selectedProvinsi]);

  const dataKecamatan = React.useMemo(() => {
    return (
      dataKabupaten?.find((kabupaten) => kabupaten.name === selectedKabupaten)
        ?.kecamatan || []
    );
  }, [selectedKabupaten, dataKabupaten]);

  return (
    <Row style={{ height: "85vh" }} justify="center">
      <Col span={18} style={{ paddingTop: "150px" }}>
        <Form
          name="basic"
          layout="horizontal"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            labelCol={{ span: 6 }}
            labelAlign="left"
            label="Jenis Transaksi"
            name="transaction-option"
            rules={[
              { required: true, message: "Please choose your transaction!" }
            ]}
          >
            <Select
              placeholder="Pilih Jenis Transaksi"
              onChange={(value) => {
                console.log("value >> ", value);
              }}
            >
              {TransactionOptionsValue.map((option) =>
                option.key === "divider" ? (
                  <Option
                    key={option.key}
                    value={option.value}
                    disabled={option.isDisabled}
                  >
                    <Divider type="horizontal" />
                  </Option>
                ) : (
                  <Option
                    key={option.key}
                    value={option.value}
                    disabled={option.isDisabled}
                  >
                    {option.label}
                  </Option>
                )
              )}
            </Select>
          </Form.Item>
          <Form.Item
            labelCol={{ span: 6 }}
            labelAlign="left"
            label="Nominal Transaki"
            name="nominal"
            rules={[
              {
                required: true,
                message: "Please input your nominal transaction!",
                type: "number"
              }
            ]}
          >
            <Col>
              <Input placeholder="Rp" />
            </Col>
          </Form.Item>
          <Form.Item
            labelCol={{ span: 6 }}
            wrapperCol={{ span: 24 }}
            labelAlign="left"
            label="Alamat Saat Ini"
            name="address"
            rules={[
              {
                required: true
              }
            ]}
          >
            <Row justify="space-between" style={{ marginBottom: "10px" }}>
              <Col span={7}>
                <Select
                  placeholder="Pilih Provinsi"
                  onChange={handleSelectedProvinsi}
                >
                  {DataProvinsi.map((provinsi, index) => (
                    <Option key={index.toString()} value={provinsi.name}>
                      {provinsi.name}
                    </Option>
                  ))}
                </Select>
              </Col>
              <Col span={7}>
                <Select
                  placeholder="Pilih Kabupaten"
                  onChange={handleSelectedKabupaten}
                >
                  {dataKabupaten.map((kabupaten, index) => (
                    <Option key={index.toString()} value={kabupaten.name}>
                      {kabupaten.name}
                    </Option>
                  ))}
                </Select>
              </Col>
              <Col span={7}>
                <Select
                  placeholder="Pilih Kecamatan"
                  onChange={handleSelectedKecamatan}
                >
                  {dataKecamatan.map((kecamatan, index) => (
                    <Option key={index.toString()} value={kecamatan}>
                      {kecamatan}
                    </Option>
                  ))}
                </Select>
              </Col>
            </Row>
            <Row>
              <Input.TextArea />
            </Row>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
};

export default TransactionPage;
